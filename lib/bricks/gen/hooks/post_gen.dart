import 'dart:io';
import 'package:gen_hooks/generator/class_model.dart';
import 'package:gen_hooks/generator/component_template.dart';
import 'package:gen_hooks/generator/example_template.dart';
import 'package:gen_hooks/generator/library_resolver.dart';
import 'package:gen_hooks/generator/visitor.dart';
import 'package:mason/mason.dart';

void run(HookContext context) async {
  final String type = context.vars["type"];
  List<dynamic> componentsAux = context.vars["components"];
  final components = componentsAux.map((e) => e.toString());
  for (final component in components) {
    String filePath = Uri.parse(
      '/lib/mwidgets/lib/src/${type}s/${component}/${component}_specs.dart',
    ).path;

    late Progress progress;

    try {
      progress = context.logger.progress(
        "Resolving component G${component.pascalCase}",
      );
      final element = await LibraryResolver.resolveLibraryElement(filePath);
      progress.complete();
      progress = context.logger.progress('Analyzing ...');
      final visitor = Visitor();
      element.visitChildren(visitor);
      progress.complete();
      progress = context.logger.progress('Writing component styles');
      await writeComponent(component, type, visitor.classModel);
      progress.complete();
      progress = context.logger.progress('Writing component examples');
      await writeExample(component, type, visitor.classModel);
      progress.complete();
    } on Exception catch (err) {
      context.logger.alert("Error in component G${component.pascalCase} \n");
      context.logger.err(err.toString());
    } finally {
      progress.complete();
    }
  }

  context.logger.success("Finalized");
}

Future<void> writeComponent(String name, String type, Class classModel) async {
  final path = Uri.parse(
    'lib/mwidgets/lib/src/${type}s/${name}/${name}.dart',
  ).path;
  final file = File(path);
  final template = ComponentTemplate(classModel: classModel, name: name);
  file.writeAsStringSync(template.toDart());
  await Process.run("dart", ["format", path]);
}

Future<void> writeExample(String name, String type, Class classModel) async {
  final path = Uri.parse(
    'lib/mwidgets_examples/lib/components/src/${type}s/${name}/${name}_examples.gen.dart',
  ).path;
  final file = File(path);
  final template = ExampleTemplate(
    classModel: classModel,
    name: name,
    type: type,
  );
  file.writeAsStringSync(template.toDart());
  await Process.run("dart", ["format", path]);
}
