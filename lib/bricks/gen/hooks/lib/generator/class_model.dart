class Class {
  List<String> generics = [];
  String documentation = '';
  List<Arguments> constructrArguments = [];
  List<String> styles = [];
  List<String> imports = [];
}

class Arguments {
  String name = '';
  String type = '';
  String defaultValue = '';
  bool isRequired = false;
  Arguments({
    required this.name,
    required this.type,
    required this.defaultValue,
    required this.isRequired,
  });
}
