import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/visitor.dart';

import 'class_model.dart';

class Visitor extends RecursiveElementVisitor {
  Class classModel;

  Visitor() : classModel = Class();

  @override
  visitTypeParameterElement(TypeParameterElement element) {
    classModel.generics.add(element.name);
    return super.visitTypeParameterElement(element);
  }

  @override
  visitLibraryImportElement(LibraryImportElement element) {
    var uri = element.uri as DirectiveUriWithLibrary;
    classModel.imports.add(uri.relativeUriString);
    return super.visitLibraryImportElement(element);
  }

  @override
  visitMethodElement(MethodElement element) {
    if (element.name.endsWith("Style")) {
      classModel.styles.add(element.name.replaceAll("Style", ""));
    } else {
      throw 'The function ${element.name} must ends with Style';
    }
    return super.visitMethodElement(element);
  }

  @override
  visitConstructorElement(ConstructorElement element) {
    classModel.documentation = element.documentationComment ?? "";
    classModel.constructrArguments = _getArguments(element.parameters);
    return super.visitConstructorElement(element);
  }

  List<Arguments> _getArguments(List<ParameterElement> parameters) {
    return parameters
        .map(
          (param) => Arguments(
            name: param.name,
            defaultValue: param.defaultValueCode ?? "",
            isRequired: param.isRequiredNamed,
            type: param.type.toString(),
          ),
        )
        .toList();
  }
}
