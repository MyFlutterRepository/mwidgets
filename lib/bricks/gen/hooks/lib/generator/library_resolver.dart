import 'dart:io';

import 'package:analyzer/dart/analysis/analysis_context_collection.dart';
import 'package:analyzer/dart/analysis/results.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/file_system/overlay_file_system.dart';
import 'package:analyzer/file_system/physical_file_system.dart';
import 'package:path/path.dart' as p;

abstract class LibraryResolver {
  static Future<LibraryElement> resolveLibraryElement(String filePath) async {
    filePath = p.normalize(p.current + filePath);

    final widget = File(filePath);
    String content = widget.readAsStringSync();

    final collection = AnalysisContextCollection(
      includedPaths: [filePath],
      resourceProvider: OverlayResourceProvider(PhysicalResourceProvider())
        ..setOverlay(
          filePath,
          content: content,
          modificationStamp: 0,
        ),
    );

    final analysisSession = collection.contextFor(filePath).currentSession;

    final libraryElement = (await analysisSession.getResolvedLibrary(filePath)
            as ResolvedLibraryResult)
        .element;

    return libraryElement;
  }
}
