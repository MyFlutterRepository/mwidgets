import 'dart:io';
import 'package:gen_hooks/choose_options.dart';
import 'package:mason/mason.dart';

void run(HookContext context) async {
  final dir = Directory('.');
  final List<FileSystemEntity> entities = await dir.list().toList();
  final entitiesName = entities.map((e) => e.uri);
  if (!entitiesName.contains(Uri.parse(r"./.is_root"))) {
    context.logger.alert(
      "Certifies that you are in root directory (contains the lib/mwidgets.dart)!! ",
    );
    exit(1);
  }

  final String type = context.vars["type"];
  final cachePath = Uri.parse('./lib/bricks/${type}s_cache.txt').path;
  final file = File(cachePath);
  final cache = file.readAsLinesSync();

  final components = ChooseOptions.chooseOne(
      values: cache
          .where(
            (name) => name.isNotEmpty,
          )
          .toList());
  if (components.isEmpty) {
    context.logger.warn(
      "\nNo components was selected, exiting. ",
    );
    exit(1);
  } else {
    context.vars["components"] = components;
  }
}
