import 'dart:io';

import 'package:mason/mason.dart';
import 'package:widget_hooks/updates.dart';

void run(HookContext context) async {
  final String type = context.vars["type"];
  final String name = context.vars["name"];

  Progress progress = context.logger.progress("Update component export");
  await updateWidgetExport(type, name);
  progress.complete();

  progress = context.logger.progress("Update example export");
  await updateExampleExport(type, name);
  progress.complete();

  progress = context.logger.progress("Update components list");
  await updateComponentsList(type, name);
  progress.complete();

  progress = context.logger.progress("Saving component");
  final cachePath = Uri.parse(
    './lib/bricks/${type}_cache.txt',
  ).path;
  final file = File(cachePath);
  final cache = file.readAsStringSync();
  file.writeAsStringSync("$cache${name.snakeCase}\n");
  progress.complete();

  context.logger.success("Finished");
}
