import 'dart:convert';
import 'dart:io';

import 'package:mason/mason.dart';

Future<void> updateWidgetExport(String type, String name) async {
  final path = Uri.parse(
    './lib/mwidgets/lib/exports/$type.dart',
  ).path;
  final export = File(path);
  String content = export.readAsStringSync();
  if (!content.contains(name.snakeCase)) {
    content +=
        "export '../src/$type/${name.snakeCase}/${name.snakeCase}.dart';";
  }
  export.writeAsStringSync(content);
  await Process.run("dart", ["format", path]);
}

Future<void> updateExampleExport(String type, String name) async {
  final path = Uri.parse(
    './lib/mwidgets_examples/lib/components/src/$type/$type.dart',
  ).path;
  final export = File(path);
  String content = export.readAsStringSync();
  if (!content.contains(name.snakeCase)) {
    content += "export './${name.snakeCase}/${name.snakeCase}_examples.dart';";
  }
  export.writeAsStringSync(content);
  await Process.run("dart", ["format", path]);
}

Future<void> updateComponentsList(String type, String name) async {
  final path = Uri.parse(
    './lib/mwidgets_examples/lib/components/components.dart',
  ).path;
  final components = File(path);
  String content = components.readAsStringSync();
  final lines = LineSplitter().convert(content);
  for (int line = 5; line < lines.length; line++) {
    if (lines[line].contains(type)) {
      lines[line + 1] = lines[line + 1].replaceFirst(
        "return [",
        "return [ M${name.pascalCase}Examples(),",
      );
    }
  }
  components.writeAsStringSync(lines.join('\n'));
  await Process.run("dart", ["format", path]);
}
