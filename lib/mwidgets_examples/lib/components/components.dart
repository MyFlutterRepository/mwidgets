import './src/atoms/atoms.dart';
import './src/molecules/molecules.dart';
import './src/organisms/organisms.dart';
import 'component_example.dart';
import 'src/tokens/colors.dart';

class Components {
  static List<ComponentExample> get tokens {
    return [
      ColorsTokens(),
    ];
  }

  static List<ComponentExample> get atoms {
    return [
      MIconExamples(),
      MLabelExamples(),
      MCheckExamples(),
    ];
  }

  static List<ComponentExample> get molecules {
    return [
      MTextFieldExamples(),
      MButtonExamples(),
      MCheckboxTileExamples(),
    ];
  }

  static List<ComponentExample> get organisms {
    return [
      MExpandableListExamples(),
      MLogoTodoAppExamples(),
    ];
  }
}
