import 'package:flutter/material.dart';
import 'package:mwidgets/core/behaviour/behaviour.dart';

import 'package:mwidgets/exports/tokens.dart';

import '../../component_example.dart';

class ColorsTokens extends ComponentExample {
  @override
  String get getName => "Colors";

  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return {
      "background1": const _ColorsBox(MColors.background1),
      "background2": const _ColorsBox(MColors.background2),
      "blue1": const _ColorsBox(MColors.blue1),
      "blue2": const _ColorsBox(MColors.blue2),
      "black": const _ColorsBox(MColors.black),
      "red1": const _ColorsBox(MColors.red1),
    };
  }
}

class _ColorsBox extends StatelessWidget {
  final Color color;
  const _ColorsBox(this.color);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 100,
        width: 100,
        color: color,
      ),
    );
  }
}
