import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/organisms.dart';

class LogoTodoAppExampleArgument {
  Behaviour behaviour;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  LogoTodoAppExampleArgument({
    required this.behaviour,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
  });
}

abstract class LogoTodoAppExample {
  LogoTodoAppExample._();

  static Map<String, Widget> examples({
    required LogoTodoAppExampleArgument defaultArgs,
    Map<String, LogoTodoAppExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MLogoTodoApp.standard(
        behaviour: argsBuild('standard').behaviour,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'MLogoTodoApp';

  static LogoTodoAppExampleArgument _factory(
    LogoTodoAppExampleArgument defaultArgs,
    Map<String, LogoTodoAppExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
