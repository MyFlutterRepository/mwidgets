import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';


import '../../../component_example.dart';
import 'logo_todo_app_examples.gen.dart';


class MLogoTodoAppExamples extends ComponentExample {

  @override
  Map<String, Widget> examples({
    required Behaviour behaviour, 
    BuildContext? context,
  }) {
    return LogoTodoAppExample.examples(
      defaultArgs: LogoTodoAppExampleArgument(
        behaviour: behaviour,
      ),
    );
  }

  @override
  String get getName => LogoTodoAppExample.name;
}
