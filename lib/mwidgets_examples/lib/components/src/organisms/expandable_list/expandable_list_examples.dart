import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/organisms.dart';

import '../../../component_example.dart';
import 'expandable_list_examples.gen.dart';

class MExpandableListExamples extends ComponentExample {
  final listItens = [
    CheckboxItem(name: 'Test check', isChecked: true),
    CheckboxItem(name: 'Test check 2', isChecked: true),
    CheckboxItem(name: 'Test check 3', isChecked: false),
    CheckboxItem(name: 'Test check 4', isChecked: true),
    CheckboxItem(name: 'Test check 5', isChecked: false),
  ];

  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return ExpandableListExample.examples(
      defaultArgs: ExpandableListExampleArgument(
        behaviour: behaviour,
        lists: [
          ExpandableListItem(
            listName: 'Lista 1',
            listItens: listItens,
          ),
          ExpandableListItem(
            listName: 'Lista 2',
            listItens: listItens,
          ),
          ExpandableListItem(
            listName: 'Lista 3',
            listItens: listItens,
          ),
        ],
      ),
    );
  }

  @override
  String get getName => ExpandableListExample.name;
}
