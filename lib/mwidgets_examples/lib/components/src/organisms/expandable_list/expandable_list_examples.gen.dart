import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/organisms.dart';

class ExpandableListExampleArgument {
  Behaviour behaviour;
  List<ExpandableListItem> lists;
  String? labelSemantics;
  String? hintSemantics;
  double height;
  Key? key;
  ExpandableListExampleArgument({
    required this.behaviour,
    required this.lists,
    this.labelSemantics,
    this.hintSemantics,
    this.height = double.infinity,
    this.key,
  });
}

abstract class ExpandableListExample {
  ExpandableListExample._();

  static Map<String, Widget> examples({
    required ExpandableListExampleArgument defaultArgs,
    Map<String, ExpandableListExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MExpandableList.standard(
        behaviour: argsBuild('standard').behaviour,
        lists: argsBuild('standard').lists,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'MExpandableList';

  static ExpandableListExampleArgument _factory(
    ExpandableListExampleArgument defaultArgs,
    Map<String, ExpandableListExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
