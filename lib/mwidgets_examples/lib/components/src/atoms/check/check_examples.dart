import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'check_examples.gen.dart';

class MCheckExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return CheckExample.examples(
      defaultArgs: CheckExampleArgument(
        behaviour: behaviour,
      ),
    ).map((key, value) => MapEntry(key, Center(child: value)));
  }

  @override
  String get getName => CheckExample.name;
}
