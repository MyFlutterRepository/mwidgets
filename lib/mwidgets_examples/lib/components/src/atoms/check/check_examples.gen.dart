import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/atoms.dart';

class CheckExampleArgument {
  Behaviour behaviour;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  CheckExampleArgument({
    required this.behaviour,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
  });
}

abstract class CheckExample {
  CheckExample._();

  static Map<String, Widget> examples({
    required CheckExampleArgument defaultArgs,
    Map<String, CheckExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MCheck.standard(
        behaviour: argsBuild('standard').behaviour,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'MCheck';

  static CheckExampleArgument _factory(
    CheckExampleArgument defaultArgs,
    Map<String, CheckExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
