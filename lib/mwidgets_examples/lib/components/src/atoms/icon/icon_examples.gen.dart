import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/atoms.dart';

class IconExampleArgument {
  Behaviour behaviour;
  IconData icon;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  IconExampleArgument({
    required this.behaviour,
    required this.icon,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
  });
}

abstract class IconExample {
  IconExample._();

  static Map<String, Widget> examples({
    required IconExampleArgument defaultArgs,
    Map<String, IconExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MIcon.standard(
        behaviour: argsBuild('standard').behaviour,
        icon: argsBuild('standard').icon,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'MIcon';

  static IconExampleArgument _factory(
    IconExampleArgument defaultArgs,
    Map<String, IconExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
