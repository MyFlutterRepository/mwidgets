import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'icon_examples.gen.dart';

class MIconExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return IconExample.examples(
      defaultArgs: IconExampleArgument(
        behaviour: behaviour,
        icon: Icons.add,
      ),
    );
  }

  @override
  String get getName => IconExample.name;
}
