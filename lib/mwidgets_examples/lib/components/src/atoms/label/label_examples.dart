import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'label_examples.gen.dart';
import 'package:mwidgets/exports/tokens.dart';

class MLabelExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return LabelExample.examples(
      defaultArgs: LabelExampleArgument(
        behaviour: behaviour,
        text: 'Texto de exemplo',
        textColor: MColors.blue1,
      ),
    );
  }

  @override
  String get getName => LabelExample.name;
}
