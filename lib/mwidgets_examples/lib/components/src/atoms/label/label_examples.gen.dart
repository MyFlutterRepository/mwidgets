import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/atoms.dart';

class LabelExampleArgument {
  Behaviour behaviour;
  String text;
  int? maxLines;
  TextAlign? textAlign;
  TextDirection? textDirection;
  TextOverflow? textOverflow;
  String? labelSemantics;
  String? hintSemantics;
  Color? textColor;
  Key? key;
  LabelExampleArgument({
    required this.behaviour,
    required this.text,
    this.maxLines,
    this.textAlign,
    this.textDirection,
    this.textOverflow,
    this.labelSemantics,
    this.hintSemantics,
    this.textColor,
    this.key,
  });
}

abstract class LabelExample {
  LabelExample._();

  static Map<String, Widget> examples({
    required LabelExampleArgument defaultArgs,
    Map<String, LabelExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'h1RegularRobot': MLabel.h1RegularRobot(
        behaviour: argsBuild('h1RegularRobot').behaviour,
        text: argsBuild('h1RegularRobot').text,
        maxLines: argsBuild('h1RegularRobot').maxLines,
        textAlign: argsBuild('h1RegularRobot').textAlign,
        textDirection: argsBuild('h1RegularRobot').textDirection,
        textOverflow: argsBuild('h1RegularRobot').textOverflow,
        labelSemantics: argsBuild('h1RegularRobot').labelSemantics,
        hintSemantics: argsBuild('h1RegularRobot').hintSemantics,
        textColor: argsBuild('h1RegularRobot').textColor,
        key: argsBuild('h1RegularRobot').key,
      ),
      'body2MediumRoboto': MLabel.body2MediumRoboto(
        behaviour: argsBuild('body2MediumRoboto').behaviour,
        text: argsBuild('body2MediumRoboto').text,
        maxLines: argsBuild('body2MediumRoboto').maxLines,
        textAlign: argsBuild('body2MediumRoboto').textAlign,
        textDirection: argsBuild('body2MediumRoboto').textDirection,
        textOverflow: argsBuild('body2MediumRoboto').textOverflow,
        labelSemantics: argsBuild('body2MediumRoboto').labelSemantics,
        hintSemantics: argsBuild('body2MediumRoboto').hintSemantics,
        textColor: argsBuild('body2MediumRoboto').textColor,
        key: argsBuild('body2MediumRoboto').key,
      ),
      'h1RegularUnderlineRobot': MLabel.h1RegularUnderlineRobot(
        behaviour: argsBuild('h1RegularUnderlineRobot').behaviour,
        text: argsBuild('h1RegularUnderlineRobot').text,
        maxLines: argsBuild('h1RegularUnderlineRobot').maxLines,
        textAlign: argsBuild('h1RegularUnderlineRobot').textAlign,
        textDirection: argsBuild('h1RegularUnderlineRobot').textDirection,
        textOverflow: argsBuild('h1RegularUnderlineRobot').textOverflow,
        labelSemantics: argsBuild('h1RegularUnderlineRobot').labelSemantics,
        hintSemantics: argsBuild('h1RegularUnderlineRobot').hintSemantics,
        textColor: argsBuild('h1RegularUnderlineRobot').textColor,
        key: argsBuild('h1RegularUnderlineRobot').key,
      ),
      'h2SemiboldRobot': MLabel.h2SemiboldRobot(
        behaviour: argsBuild('h2SemiboldRobot').behaviour,
        text: argsBuild('h2SemiboldRobot').text,
        maxLines: argsBuild('h2SemiboldRobot').maxLines,
        textAlign: argsBuild('h2SemiboldRobot').textAlign,
        textDirection: argsBuild('h2SemiboldRobot').textDirection,
        textOverflow: argsBuild('h2SemiboldRobot').textOverflow,
        labelSemantics: argsBuild('h2SemiboldRobot').labelSemantics,
        hintSemantics: argsBuild('h2SemiboldRobot').hintSemantics,
        textColor: argsBuild('h2SemiboldRobot').textColor,
        key: argsBuild('h2SemiboldRobot').key,
      ),
    };
  }

  static get name => 'MLabel';

  static LabelExampleArgument _factory(
    LabelExampleArgument defaultArgs,
    Map<String, LabelExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
