import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/molecules.dart';

class TextFieldExampleArgument {
  Key? key;
  Behaviour behaviour;
  bool obscureText;
  bool readOnly;
  TextCapitalization textCapitalization;
  String? labelSemantics;
  String? hintSemantics;
  dynamic Function(String?)? onChange;
  FocusNode? focusNode;
  TextEditingController? controller;
  bool? enabled;
  List<TextInputFormatter>? inputFormatters;
  Widget? Function(BuildContext,
      {required int currentLength,
      required bool isFocused,
      required int? maxLength})? buildCounter;
  TextInputType? keyboardType;
  String? errorText;
  String? hintText;
  String? helperText;
  String? label;
  Widget? prefixIcon;
  Widget? suffix;
  int? maxLength;
  int? maxLines;
  TextFieldExampleArgument({
    this.key,
    required this.behaviour,
    this.obscureText = false,
    this.readOnly = false,
    this.textCapitalization = TextCapitalization.none,
    this.labelSemantics,
    this.hintSemantics,
    this.onChange,
    this.focusNode,
    this.controller,
    this.enabled,
    this.inputFormatters,
    this.buildCounter,
    this.keyboardType,
    this.errorText,
    this.hintText,
    this.helperText,
    this.label,
    this.prefixIcon,
    this.suffix,
    this.maxLength,
    this.maxLines,
  });
}

abstract class TextFieldExample {
  TextFieldExample._();

  static Map<String, Widget> examples({
    required TextFieldExampleArgument defaultArgs,
    Map<String, TextFieldExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MTextField.standard(
        key: argsBuild('standard').key,
        behaviour: argsBuild('standard').behaviour,
        obscureText: argsBuild('standard').obscureText,
        readOnly: argsBuild('standard').readOnly,
        textCapitalization: argsBuild('standard').textCapitalization,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        onChange: argsBuild('standard').onChange,
        focusNode: argsBuild('standard').focusNode,
        controller: argsBuild('standard').controller,
        enabled: argsBuild('standard').enabled,
        inputFormatters: argsBuild('standard').inputFormatters,
        buildCounter: argsBuild('standard').buildCounter,
        keyboardType: argsBuild('standard').keyboardType,
        errorText: argsBuild('standard').errorText,
        hintText: argsBuild('standard').hintText,
        helperText: argsBuild('standard').helperText,
        label: argsBuild('standard').label,
        prefixIcon: argsBuild('standard').prefixIcon,
        suffix: argsBuild('standard').suffix,
        maxLength: argsBuild('standard').maxLength,
        maxLines: argsBuild('standard').maxLines,
      ),
      'expandedStandard': MTextField.expandedStandard(
        key: argsBuild('expandedStandard').key,
        behaviour: argsBuild('expandedStandard').behaviour,
        obscureText: argsBuild('expandedStandard').obscureText,
        readOnly: argsBuild('expandedStandard').readOnly,
        textCapitalization: argsBuild('expandedStandard').textCapitalization,
        labelSemantics: argsBuild('expandedStandard').labelSemantics,
        hintSemantics: argsBuild('expandedStandard').hintSemantics,
        onChange: argsBuild('expandedStandard').onChange,
        focusNode: argsBuild('expandedStandard').focusNode,
        controller: argsBuild('expandedStandard').controller,
        enabled: argsBuild('expandedStandard').enabled,
        inputFormatters: argsBuild('expandedStandard').inputFormatters,
        buildCounter: argsBuild('expandedStandard').buildCounter,
        keyboardType: argsBuild('expandedStandard').keyboardType,
        errorText: argsBuild('expandedStandard').errorText,
        hintText: argsBuild('expandedStandard').hintText,
        helperText: argsBuild('expandedStandard').helperText,
        label: argsBuild('expandedStandard').label,
        prefixIcon: argsBuild('expandedStandard').prefixIcon,
        suffix: argsBuild('expandedStandard').suffix,
        maxLength: argsBuild('expandedStandard').maxLength,
        maxLines: argsBuild('expandedStandard').maxLines,
      ),
      'withoutLabel': MTextField.withoutLabel(
        key: argsBuild('withoutLabel').key,
        behaviour: argsBuild('withoutLabel').behaviour,
        obscureText: argsBuild('withoutLabel').obscureText,
        readOnly: argsBuild('withoutLabel').readOnly,
        textCapitalization: argsBuild('withoutLabel').textCapitalization,
        labelSemantics: argsBuild('withoutLabel').labelSemantics,
        hintSemantics: argsBuild('withoutLabel').hintSemantics,
        onChange: argsBuild('withoutLabel').onChange,
        focusNode: argsBuild('withoutLabel').focusNode,
        controller: argsBuild('withoutLabel').controller,
        enabled: argsBuild('withoutLabel').enabled,
        inputFormatters: argsBuild('withoutLabel').inputFormatters,
        buildCounter: argsBuild('withoutLabel').buildCounter,
        keyboardType: argsBuild('withoutLabel').keyboardType,
        errorText: argsBuild('withoutLabel').errorText,
        hintText: argsBuild('withoutLabel').hintText,
        helperText: argsBuild('withoutLabel').helperText,
        label: argsBuild('withoutLabel').label,
        prefixIcon: argsBuild('withoutLabel').prefixIcon,
        suffix: argsBuild('withoutLabel').suffix,
        maxLength: argsBuild('withoutLabel').maxLength,
        maxLines: argsBuild('withoutLabel').maxLines,
      ),
    };
  }

  static get name => 'MTextField';

  static TextFieldExampleArgument _factory(
    TextFieldExampleArgument defaultArgs,
    Map<String, TextFieldExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
