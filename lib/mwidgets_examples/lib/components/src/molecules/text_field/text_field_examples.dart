import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'text_field_examples.gen.dart';

class MTextFieldExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return TextFieldExample.examples(
      defaultArgs: TextFieldExampleArgument(
        behaviour: behaviour,
        helperText: "Esse é o text de ajuda",
        hintText: "Esse é o hint text",
        label: "Label do componente",
      ),
    ).map(
      (key, value) => MapEntry(
        key,
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: value,
        ),
      ),
    );
  }

  @override
  String get getName => TextFieldExample.name;
}
