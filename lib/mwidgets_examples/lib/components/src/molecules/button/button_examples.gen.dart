import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/molecules.dart';

class ButtonExampleArgument {
  Behaviour behaviour;
  dynamic Function()? onPressed;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  IconData? icon;
  String? text;
  IconData? trailingWidget;
  ButtonExampleArgument({
    required this.behaviour,
    this.onPressed,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
    this.icon,
    this.text,
    this.trailingWidget,
  });
}

abstract class ButtonExample {
  ButtonExample._();

  static Map<String, Widget> examples({
    required ButtonExampleArgument defaultArgs,
    Map<String, ButtonExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'textH1RegularRoboto': MButton.textH1RegularRoboto(
        behaviour: argsBuild('textH1RegularRoboto').behaviour,
        onPressed: argsBuild('textH1RegularRoboto').onPressed,
        labelSemantics: argsBuild('textH1RegularRoboto').labelSemantics,
        hintSemantics: argsBuild('textH1RegularRoboto').hintSemantics,
        key: argsBuild('textH1RegularRoboto').key,
        icon: argsBuild('textH1RegularRoboto').icon,
        text: argsBuild('textH1RegularRoboto').text,
        trailingWidget: argsBuild('textH1RegularRoboto').trailingWidget,
      ),
      'textH1RegularUnderlineRoboto': MButton.textH1RegularUnderlineRoboto(
        behaviour: argsBuild('textH1RegularUnderlineRoboto').behaviour,
        onPressed: argsBuild('textH1RegularUnderlineRoboto').onPressed,
        labelSemantics:
            argsBuild('textH1RegularUnderlineRoboto').labelSemantics,
        hintSemantics: argsBuild('textH1RegularUnderlineRoboto').hintSemantics,
        key: argsBuild('textH1RegularUnderlineRoboto').key,
        icon: argsBuild('textH1RegularUnderlineRoboto').icon,
        text: argsBuild('textH1RegularUnderlineRoboto').text,
        trailingWidget:
            argsBuild('textH1RegularUnderlineRoboto').trailingWidget,
      ),
      'icon16WhiteBlue1Circular': MButton.icon16WhiteBlue1Circular(
        behaviour: argsBuild('icon16WhiteBlue1Circular').behaviour,
        onPressed: argsBuild('icon16WhiteBlue1Circular').onPressed,
        labelSemantics: argsBuild('icon16WhiteBlue1Circular').labelSemantics,
        hintSemantics: argsBuild('icon16WhiteBlue1Circular').hintSemantics,
        key: argsBuild('icon16WhiteBlue1Circular').key,
        icon: argsBuild('icon16WhiteBlue1Circular').icon,
        text: argsBuild('icon16WhiteBlue1Circular').text,
        trailingWidget: argsBuild('icon16WhiteBlue1Circular').trailingWidget,
      ),
    };
  }

  static get name => 'MButton';

  static ButtonExampleArgument _factory(
    ButtonExampleArgument defaultArgs,
    Map<String, ButtonExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
