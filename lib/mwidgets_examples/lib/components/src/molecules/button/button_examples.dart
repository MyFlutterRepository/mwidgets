import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'button_examples.gen.dart';

class MButtonExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return ButtonExample.examples(
      defaultArgs: ButtonExampleArgument(
        behaviour: behaviour,
        onPressed: () => debugPrint("Botão apertado"),
        text: "Entrar",
        icon: Icons.add,
      ),
    );
  }

  @override
  String get getName => ButtonExample.name;
}
