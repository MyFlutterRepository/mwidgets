import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

import '../../../component_example.dart';
import 'checkbox_tile_examples.gen.dart';

class MCheckboxTileExamples extends ComponentExample {
  @override
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  }) {
    return CheckboxTileExample.examples(
      defaultArgs: CheckboxTileExampleArgument(
        behaviour: behaviour,
        text: 'Um lindo checkbox',
        onChange: (value) => debugPrint('Print checkbox $value'),
      ),
    );
  }

  @override
  String get getName => CheckboxTileExample.name;
}
