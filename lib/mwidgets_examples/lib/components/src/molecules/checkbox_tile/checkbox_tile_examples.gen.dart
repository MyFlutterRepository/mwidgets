import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/molecules.dart';

class CheckboxTileExampleArgument {
  Behaviour behaviour;
  String text;
  dynamic Function(bool)? onChange;
  bool initialChecked;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  CheckboxTileExampleArgument({
    required this.behaviour,
    required this.text,
    this.onChange,
    this.initialChecked = false,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
  });
}

abstract class CheckboxTileExample {
  CheckboxTileExample._();

  static Map<String, Widget> examples({
    required CheckboxTileExampleArgument defaultArgs,
    Map<String, CheckboxTileExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': MCheckboxTile.standard(
        behaviour: argsBuild('standard').behaviour,
        text: argsBuild('standard').text,
        onChange: argsBuild('standard').onChange,
        initialChecked: argsBuild('standard').initialChecked,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'MCheckboxTile';

  static CheckboxTileExampleArgument _factory(
    CheckboxTileExampleArgument defaultArgs,
    Map<String, CheckboxTileExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
