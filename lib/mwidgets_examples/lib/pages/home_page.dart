import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'tab_view_components_page.dart';
import '../components/components.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _currentBehaviour = 0;

  List<Map<String, dynamic>> get _behaviours {
    final base = [
      {
        "type": Behaviour.regular,
        "name": "Regular",
      },
      {
        "type": Behaviour.error,
        "name": "Error",
      },
      {
        "type": Behaviour.empty,
        "name": "Empty",
      },
      {
        "type": Behaviour.loading,
        "name": "Loading",
      },
      {
        "type": Behaviour.disabled,
        "name": "Disabled",
      },
    ];

    return base;
  }

  void _changeBehaviour() {
    if (_currentBehaviour + 1 == _behaviours.length) {
      _currentBehaviour = 0;
    } else {
      _currentBehaviour++;
    }
    setState(() {});
  }

  Behaviour get _behaviour => _behaviours[_currentBehaviour]["type"];
  String get _behaviourName => _behaviours[_currentBehaviour]["name"];

  static const List<Tab> tabs = <Tab>[
    Tab(text: 'Tokens'),
    Tab(text: 'Atoms'),
    Tab(text: 'Molecules'),
    Tab(text: 'Organisms'),
  ];

  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Design System",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24,
          ),
        ),
        bottom: TabBar(
          unselectedLabelColor: Colors.white54,
          labelColor: Colors.white,
          indicatorColor: Colors.white,
          controller: tabController,
          tabs: tabs,
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          TabViewComponentsPage(
            behaviour: _behaviour,
            components: Components.tokens,
          ),
          TabViewComponentsPage(
            behaviour: _behaviour,
            components: Components.atoms,
          ),
          TabViewComponentsPage(
            behaviour: _behaviour,
            components: Components.molecules,
          ),
          TabViewComponentsPage(
            behaviour: _behaviour,
            components: Components.organisms,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _changeBehaviour,
        tooltip: 'Behaviour',
        label: Text(
          _behaviourName,
          style: const TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
