import 'package:flutter/material.dart';

import 'package:mwidgets/core/core.dart';

import '../components/component_example.dart';

class TabViewComponentsPage extends StatefulWidget {
  final Behaviour behaviour;
  final List<ComponentExample> components;

  const TabViewComponentsPage({
    Key? key,
    required this.behaviour,
    required this.components,
  }) : super(key: key);

  @override
  State<TabViewComponentsPage> createState() => _TabViewComponentsPageState();
}

class _TabViewComponentsPageState extends State<TabViewComponentsPage> {
  late String search;

  @override
  initState() {
    search = '';
    super.initState();
  }

  List<Widget> _buildComponents() {
    widget.components.sort((ComponentExample a, ComponentExample b) =>
        a.getName.compareTo(b.getName));
    return widget.components
        .where((element) => element.getName.contains(search))
        .map(
          (e) => ExpansionTile(
            title: Text(e.getName),
            children: e
                .examples(behaviour: widget.behaviour, context: context)
                .entries
                .map(
                  (e) => Container(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            e.key,
                            style: const TextStyle(
                              fontSize: 16.0,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        const SizedBox(height: 8.0),
                        Center(
                          child: e.value,
                        ),
                        const SizedBox(height: 8.0),
                        const Divider()
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 16,
          ),
          child: TextField(
            onChanged: (text) => setState(() {
              search = text;
            }),
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.search),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black54),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
            ),
          ),
        ),
        ..._buildComponents(),
      ],
    );
  }
}
