import 'behaviour.dart';

/// Utilitário que ajuda a definir quais comportamentos (Behaviours) o componente deve implementar.

class BehaviourHelper {
  static const Map<Behaviour, Behaviour> regularAndLoading = {
    Behaviour.error: Behaviour.regular,
    Behaviour.disabled: Behaviour.regular,
    Behaviour.empty: Behaviour.regular
  };

  static const Map<Behaviour, Behaviour> regularAndDisabled = {
    Behaviour.loading: Behaviour.regular,
    Behaviour.error: Behaviour.regular,
    Behaviour.empty: Behaviour.regular
  };

  static const Map<Behaviour, Behaviour> regularAndEmpty = {
    Behaviour.loading: Behaviour.regular,
    Behaviour.error: Behaviour.regular,
    Behaviour.disabled: Behaviour.regular
  };

  static const Map<Behaviour, Behaviour> regularAndLoadingAndEmpty = {
    Behaviour.error: Behaviour.regular,
    Behaviour.disabled: Behaviour.regular,
  };

  static const Map<Behaviour, Behaviour> exceptDisabled = {
    Behaviour.disabled: Behaviour.regular,
  };

  static const Map<Behaviour, Behaviour> buttons = {
    Behaviour.error: Behaviour.regular,
    Behaviour.empty: Behaviour.regular
  };

  static const Map<Behaviour, Behaviour> regularAndError = {
    Behaviour.empty: Behaviour.regular,
    Behaviour.disabled: Behaviour.regular,
    Behaviour.loading: Behaviour.regular,
  };

  static const Map<Behaviour, Behaviour> exceptEmpty = {
    Behaviour.empty: Behaviour.regular,
  };

  static const Map<Behaviour, Behaviour> regularAndErrorAndDisabled = {
    Behaviour.loading: Behaviour.regular,
    Behaviour.empty: Behaviour.regular,
  };

  static const Map<Behaviour, Behaviour> regularAndLoadingAndDisabled = {
    Behaviour.empty: Behaviour.regular,
    Behaviour.error: Behaviour.regular,
  };

  static Behaviour childBehaviour({
    required Behaviour behaviour,
    required Map<Behaviour, Behaviour>? delegate,
  }) =>
      delegate != null ? delegate[behaviour] ?? behaviour : behaviour;
}
