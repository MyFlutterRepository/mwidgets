import 'package:flutter/material.dart';
import '../behaviour/behaviour.dart';
import '../behaviour/behaviour_helper.dart';
import 'base_component.dart';
import 'component_style.dart';

/// Component é a classe pai de todos os componentes do projeto.
///
/// Define uma interface comum para que os componentes garantam propriedades
/// uniformes.
///
/// Um componente pode ser renderizado em diferentes contextos. Esses contextos
/// são chamados de Behaviour.
///
/// ### REGULAR
/// É o uso padrão do componente.
///
/// ### LOADING
/// Define como o componente deve ser quando estiver em processo de loading.
///
/// ### DISABLED
/// Define como o componente deve ser quando estiver desabilitado.
///
/// ### ERROR
/// Define como o componente deve ser mostrado
/// quando a aplicação está em um estado de erro.
///
///
/// Os componentes devem ser capazes de receber as configurações de tema externa
/// e de desabilitar as animações quando necessário.
///

abstract class Component<T, U> implements BaseComponent<T, U> {
  @override
  Widget whenError(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    throw ("$this does not implements Behaviour.error");
  }

  @override
  Widget whenDisabled(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    throw ("$this does not implements Behaviour.disabled");
  }

  @override
  Widget whenEmpty(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    throw ("$this does not implements Behaviour.empty");
  }

  ///
  /// Função que renderiza o componente baseado no behaviour atual
  ///

  @override
  Widget render({
    required BuildContext context,
    required Behaviour behaviour,
    required ComponentStyle<T, U> componentStyle,
  }) {
    final actualBehaviour = BehaviourHelper.childBehaviour(
        behaviour: behaviour, delegate: delegate);

    switch (actualBehaviour) {
      case Behaviour.regular:
        if (componentStyle.regular == null) {
          throw "$this uses $behaviour but it does not have $behaviour style defined";
        }
        return whenRegular(
          componentStyle.regular as T,
          componentStyle.shared!,
          context,
          actualBehaviour,
        );

      case Behaviour.loading:
        if (componentStyle.shared == null) {
          throw "$this uses $behaviour but it does not have $behaviour style defined";
        }
        return whenLoading(
          componentStyle.loading as T,
          componentStyle.shared!,
          context,
          actualBehaviour,
        );

      case Behaviour.error:
        if (componentStyle.error == null) {
          throw "$this uses $behaviour but it does not have $behaviour style defined";
        }
        return whenError(
          componentStyle.error as T,
          componentStyle.shared!,
          context,
          actualBehaviour,
        );

      case Behaviour.empty:
        if (componentStyle.empty == null) {
          throw "$this uses $behaviour but it does not have $behaviour style defined";
        }
        return whenEmpty(
          componentStyle.empty as T,
          componentStyle.shared!,
          context,
          actualBehaviour,
        );

      case Behaviour.disabled:
        if (componentStyle.disabled == null) {
          throw "$this uses $behaviour but it does not have $behaviour style defined";
        }
        return whenDisabled(
          componentStyle.disabled as T,
          componentStyle.shared!,
          context,
          actualBehaviour,
        );

      default:
        throw "$behaviour is not implemented for $this";
    }
  }
}
