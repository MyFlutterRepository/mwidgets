import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';

import '../../../core/component/component_style.dart';
import 'component/check_component.dart';
import 'component/check_component_style.dart';

abstract class CheckSpecs extends CheckComponent {
  /// Is a simple box with a check
  /// [initialChecked] is the value when the widget is built
  /// [onChange] is called every time that the check is pressed
  CheckSpecs({
    required super.behaviour,
    required super.componentStyle,
    super.labelSemantics,
    super.hintSemantics,
    super.initialChecked,
    super.onChange,
    super.key,
  });

  static standardStyle() =>
      ComponentStyle<CheckComponentStyle, CheckComponentSharedStyle>(
        regular: CheckComponentStyle(
          checkSize: const Size(MSizes.s24, MSizes.s24),
          checkBackground: MColors.background2,
        ),
        loading: CheckComponentStyle(
          checkSize: const Size(MSizes.s24, MSizes.s24),
          checkBackground: MColors.background2,
        ),
        error: CheckComponentStyle(
          checkSize: const Size(MSizes.s24, MSizes.s24),
          checkBackground: MColors.red1,
        ),
        shared: CheckComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(const Size(0, 0)),
        ),
      );
}
