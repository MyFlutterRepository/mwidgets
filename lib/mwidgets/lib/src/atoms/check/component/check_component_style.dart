import 'package:flutter/material.dart';

import '../../../../exports/shared.dart';

class CheckComponentStyle {
  final Color checkBackground;
  final Size checkSize;

  CheckComponentStyle({
    required this.checkBackground,
    required this.checkSize,
  });
}

class CheckComponentSharedStyle {
  final LoadingStyle loadingStyle;
  CheckComponentSharedStyle({
    required this.loadingStyle,
  });
}
