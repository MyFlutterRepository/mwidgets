import 'package:flutter/material.dart';
import '../../../../exports/shared.dart';
import '../../../../exports/tokens.dart';
import 'check_component_style.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class CheckComponent extends StatelessWidget
    with Component<CheckComponentStyle, CheckComponentSharedStyle> {
  /// Component behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<CheckComponentStyle, CheckComponentSharedStyle>
      componentStyle;

  /// Initial state
  final bool initialChecked;

  /// Function called on change
  final Function(bool check)? onChange;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  final ValueNotifier<bool> _check;

  CheckComponent({
    required this.behaviour,
    required this.componentStyle,
    this.labelSemantics,
    this.hintSemantics,
    this.initialChecked = false,
    this.onChange,
    super.key,
  }) : _check = ValueNotifier(initialChecked);

  @override
  Widget whenRegular(
    CheckComponentStyle style,
    CheckComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: GestureDetector(
        onTap: () {
          _check.value = !_check.value;
          if (onChange != null) {
            onChange!(_check.value);
          }
        },
        child: ValueListenableBuilder<bool>(
          valueListenable: _check,
          builder: (context, check, _) {
            return Container(
              decoration: BoxDecoration(
                border: Border.all(),
                borderRadius: BorderRadius.circular(
                  MSizes.s8,
                ),
                color: check ? style.checkBackground : null,
              ),
              height: style.checkSize.height,
              width: style.checkSize.width,
              child: Visibility(
                visible: check,
                child: Center(
                  child: Image.asset(
                    'lib/assets/images/png/V.png',
                    height: style.checkSize.width + 2,
                    fit: BoxFit.none,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget whenError(
    CheckComponentStyle style,
    CheckComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(),
          borderRadius: BorderRadius.circular(
            MSizes.s8,
          ),
          color: style.checkBackground,
        ),
        height: style.checkSize.height,
        width: style.checkSize.width,
      ),
    );
  }

  @override
  Widget whenLoading(
    CheckComponentStyle style,
    CheckComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: LoadingWidget(
        style: sharedStyle.loadingStyle,
        size: style.checkSize,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => {
        Behaviour.disabled: Behaviour.loading,
        Behaviour.empty: Behaviour.regular
      };
}
