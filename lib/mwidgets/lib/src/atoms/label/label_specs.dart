import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';

import '../../../core/component/component_style.dart';
import 'component/label_component.dart';
import 'component/label_component_style.dart';

abstract class LabelSpecs extends LabelComponent {
  static const _errorColor = MColors.red1;

  /// This componente shows a text in a pre-configured style
  /// [text] The component text
  /// [maxLines] The max lines of the text
  /// [textAlign] The position of the text
  /// [textDirection] From ltr or rtl
  /// [textOverflow] The behaviour of the text when it overflow
  const LabelSpecs({
    required super.behaviour,
    required super.componentStyle,
    required super.text,
    super.maxLines,
    super.textAlign,
    super.textDirection,
    super.textOverflow,
    super.labelSemantics,
    super.hintSemantics,
    super.textColor,
    super.key,
  });

  static h1RegularRobotStyle() =>
      ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>(
        regular: LabelComponentStyle(
          style: MTextStyles.h1RegularRobot,
        ),
        disabled: LabelComponentStyle(
          style: MTextStyles.h1RegularRobot,
        ),
        error: LabelComponentStyle(
          style: MTextStyles.h1RegularRobot.copyWith(color: _errorColor),
        ),
        loading: LabelComponentStyle(
          style: MTextStyles.h1RegularRobot,
        ),
        shared: LabelComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s128, 20),
          ),
        ),
      );

  /// [fontFamily] 'RobotoFlex', [fontSize] 16, [fontWeight] 700
  static body2MediumRobotoStyle() =>
      ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>(
        regular: LabelComponentStyle(
          style: MTextStyles.body2MediumRobot,
        ),
        disabled: LabelComponentStyle(
          style: MTextStyles.body2MediumRobot,
        ),
        error: LabelComponentStyle(
          style: MTextStyles.body2MediumRobot.copyWith(color: _errorColor),
        ),
        loading: LabelComponentStyle(
          style: MTextStyles.body2MediumRobot,
        ),
        shared: LabelComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s128, 20),
          ),
        ),
      );

  static h1RegularUnderlineRobotStyle() =>
      ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>(
        regular: LabelComponentStyle(
          style: MTextStyles.h1RegularUnderlineRobot,
        ),
        disabled: LabelComponentStyle(
          style: MTextStyles.h1RegularUnderlineRobot,
        ),
        error: LabelComponentStyle(
          style:
              MTextStyles.h1RegularUnderlineRobot.copyWith(color: _errorColor),
        ),
        loading: LabelComponentStyle(
          style: MTextStyles.h1RegularUnderlineRobot,
        ),
        shared: LabelComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s128, 20),
          ),
        ),
      );

  static h2SemiboldRobotStyle() =>
      ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>(
        regular: LabelComponentStyle(
          style: MTextStyles.h2SemiboldRobot,
        ),
        disabled: LabelComponentStyle(
          style: MTextStyles.h2SemiboldRobot,
        ),
        error: LabelComponentStyle(
          style: MTextStyles.h2SemiboldRobot.copyWith(color: _errorColor),
        ),
        loading: LabelComponentStyle(
          style: MTextStyles.h2SemiboldRobot,
        ),
        shared: LabelComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s128, 20),
          ),
        ),
      );
}
