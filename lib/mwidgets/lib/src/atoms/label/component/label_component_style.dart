import 'package:flutter/material.dart';

import '../../../shared/widgets/loading/loading.dart';

class LabelComponentStyle {
  final TextStyle style;

  LabelComponentStyle({
    required this.style,
  });
}

class LabelComponentSharedStyle {
  final LoadingStyle loadingStyle;

  LabelComponentSharedStyle({
    required this.loadingStyle,
  });
}
