import 'package:flutter/material.dart';
import '../../../../exports/shared.dart';
import 'label_component_style.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class LabelComponent extends StatelessWidget
    with Component<LabelComponentStyle, LabelComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>
      componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  /// Component text
  final String text;

  /// Number max of lines
  final int? maxLines;

  /// Behaviour of text when overflow
  final TextOverflow? textOverflow;

  /// The text Align
  final TextAlign? textAlign;

  /// Direction of text
  final TextDirection? textDirection;

  /// Color of text in regular behaviour
  final Color? textColor;

  const LabelComponent({
    required this.behaviour,
    required this.componentStyle,
    required this.text,
    this.labelSemantics,
    this.hintSemantics,
    this.maxLines,
    this.textAlign,
    this.textDirection,
    this.textOverflow,
    this.textColor,
    super.key,
  });

  @override
  Widget whenRegular(
    LabelComponentStyle style,
    LabelComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      Semantics(
        label: labelSemantics,
        hint: hintSemantics,
        child: Text(
          text,
          style: style.style.copyWith(
            color: textColor,
          ),
          maxLines: maxLines,
          overflow: textOverflow,
          textAlign: textAlign,
          textDirection: textDirection,
        ),
      );

  @override
  Widget whenError(
    LabelComponentStyle style,
    LabelComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      Semantics(
        label: labelSemantics,
        hint: hintSemantics,
        child: Text(
          text,
          style: style.style,
          maxLines: maxLines,
          overflow: textOverflow,
          textAlign: textAlign,
          textDirection: textDirection,
        ),
      );

  @override
  Widget whenDisabled(
    LabelComponentStyle style,
    LabelComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      Semantics(
        label: labelSemantics,
        hint: hintSemantics,
        child: Text(
          text,
          style: style.style.copyWith(
            color: textColor?.withOpacity(0.5),
          ),
          maxLines: maxLines,
          overflow: textOverflow,
          textAlign: textAlign,
          textDirection: textDirection,
        ),
      );

  @override
  Widget whenLoading(
    LabelComponentStyle style,
    LabelComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: LoadingWidget(
        style: sharedStyle.loadingStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => {
        Behaviour.empty: Behaviour.regular,
      };
}
