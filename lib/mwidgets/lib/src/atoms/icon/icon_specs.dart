import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';

import '../../../core/component/component_style.dart';
import 'component/icon_component.dart';
import 'component/icon_component_style.dart';

abstract class IconSpecs extends IconComponent {
  /// Put your comment here
  const IconSpecs({
    required super.behaviour,
    required super.componentStyle,
    required super.icon,
    super.labelSemantics,
    super.hintSemantics,
    super.key,
  });

  static standardStyle() =>
      ComponentStyle<IconComponentStyle, IconComponentSharedStyle>(
        regular: IconComponentStyle(
          iconSize: const Size(MSizes.s16, MSizes.s16),
        ),
        loading: IconComponentStyle(
          iconSize: const Size(MSizes.s16, MSizes.s16),
        ),
        shared: IconComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(const Size(0, 0)),
        ),
      );
}
