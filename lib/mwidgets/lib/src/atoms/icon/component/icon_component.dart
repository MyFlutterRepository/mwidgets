import 'package:flutter/material.dart';
import '../../../../exports/shared.dart';
import 'icon_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class IconComponent extends StatelessWidget
    with Component<IconComponentStyle, IconComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<IconComponentStyle, IconComponentSharedStyle>
      componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  /// Icon showed im the component
  final IconData icon;

  /// Icon color
  final Color? iconColor;

  const IconComponent({
    required this.behaviour,
    required this.componentStyle,
    required this.icon,
    this.labelSemantics,
    this.hintSemantics,
    this.iconColor,
    super.key,
  });

  @override
  Widget whenRegular(
    IconComponentStyle style,
    IconComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }

  @override
  Widget whenLoading(
    IconComponentStyle style,
    IconComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: LoadingWidget(
        size: style.iconSize,
        style: sharedStyle.loadingStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => BehaviourHelper.regularAndLoading;
}
