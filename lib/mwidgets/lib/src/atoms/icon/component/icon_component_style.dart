import 'package:flutter/material.dart';

import '../../../../exports/shared.dart';

class IconComponentStyle {
  final Size iconSize;

  IconComponentStyle({
    required this.iconSize,
  });
}

class IconComponentSharedStyle {
  final LoadingStyle loadingStyle;
  IconComponentSharedStyle({
    required this.loadingStyle,
  });
}
