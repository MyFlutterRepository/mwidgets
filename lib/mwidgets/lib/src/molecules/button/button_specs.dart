import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';
import '../../atoms/label/label_specs.dart';

import '../../../core/component/component_style.dart';
import '../../atoms/icon/icon_specs.dart';
import 'component/button_component.dart';
import 'component/button_component_style.dart';

abstract class ButtonSpecs extends ButtonComponent {
  /// Put your comment here
  ButtonSpecs({
    required super.behaviour,
    required super.componentStyle,
    super.onPressed,
    super.labelSemantics,
    super.hintSemantics,
    super.key,
    super.icon,
    super.text,
    super.trailingWidget,
  });

  static textH1RegularRobotoStyle() =>
      ComponentStyle<ButtonComponentStyle, ButtonComponentSharedStyle>(
        regular: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            backgroundColor: MColors.blue1,
            foregroundColor: MColors.white,
            shadowColor: MColors.blue2,
            minimumSize: const Size(MSizes.s1, MSizes.s48),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(MSizes.s8),
            ),
          ),
          buttonType: ButtonType.text,
        ),
        disabled: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            minimumSize: const Size(MSizes.s1, MSizes.s48),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(MSizes.s8),
            ),
          ),
          buttonType: ButtonType.text,
        ),
        loading: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(),
          buttonType: ButtonType.text,
        ),
        shared: ButtonComponentSharedStyle(
          loadingStyle:
              LoadingSpecs.standardStyle(const Size(MSizes.s128, MSizes.s48)),
          iconStyle: IconSpecs.standardStyle(),
          textStyle: LabelSpecs.h1RegularRobotStyle(),
        ),
      );

  static textH1RegularUnderlineRobotoStyle() =>
      ComponentStyle<ButtonComponentStyle, ButtonComponentSharedStyle>(
        regular: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            backgroundColor: MColors.transparent,
            foregroundColor: MColors.blue1,
            shadowColor: MColors.transparent,
            elevation: 0,
            minimumSize: const Size(MSizes.s1, MSizes.s24),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(MSizes.s8),
            ),
          ),
          buttonType: ButtonType.text,
        ),
        disabled: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            disabledBackgroundColor: MColors.transparent,
            minimumSize: const Size(MSizes.s1, MSizes.s24),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(MSizes.s8),
            ),
          ),
          buttonType: ButtonType.text,
        ),
        loading: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(),
          buttonType: ButtonType.text,
        ),
        shared: ButtonComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s128, MSizes.s24),
          ),
          iconStyle: IconSpecs.standardStyle(),
          textStyle: LabelSpecs.h1RegularUnderlineRobotStyle(),
        ),
      );

  static icon16WhiteBlue1CircularStyle() =>
      ComponentStyle<ButtonComponentStyle, ButtonComponentSharedStyle>(
        regular: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            backgroundColor: MColors.blue1,
            foregroundColor: MColors.white,
            minimumSize: const Size(MSizes.s40, MSizes.s40),
            shadowColor: MColors.blue2,
            shape: const CircleBorder(),
            padding: EdgeInsets.zero,
          ),
          buttonType: ButtonType.icon,
        ),
        disabled: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(
            minimumSize: const Size(MSizes.s40, MSizes.s40),
            shape: const CircleBorder(),
            padding: EdgeInsets.zero,
          ),
          buttonType: ButtonType.icon,
        ),
        loading: ButtonComponentStyle(
          buttonStyle: ElevatedButton.styleFrom(),
          buttonType: ButtonType.icon,
        ),
        shared: ButtonComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(MSizes.s40, MSizes.s40),
          ),
          iconStyle: IconSpecs.standardStyle(),
          textStyle: LabelSpecs.h1RegularRobotStyle(),
        ),
      );
}
