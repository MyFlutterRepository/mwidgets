import 'package:flutter/material.dart';
import '../../../../exports/shared.dart';
import '../../../../exports/tokens.dart';
import '../../../atoms/icon/component/icon_component.dart';
import '../../../atoms/label/component/label_component.dart';
import 'button_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class ButtonComponent extends StatelessWidget
    with Component<ButtonComponentStyle, ButtonComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<ButtonComponentStyle, ButtonComponentSharedStyle>
      componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  /// Function executed when button is pressed
  final Function()? onPressed;

  /// Text showed in the text button type
  final String? text;

  /// The Icon showed before the text
  final IconData? trailingWidget;

  /// Icon showed in icon button text
  final IconData? icon;

  ButtonComponent({
    super.key,
    required this.behaviour,
    required this.componentStyle,
    this.labelSemantics,
    this.hintSemantics,
    this.onPressed,
    this.text,
    this.trailingWidget,
    this.icon,
  })  : assert(
          componentStyle.regular!.buttonType != ButtonType.text || text != null,
          "The text must be != null when the button type is text",
        ),
        assert(
          componentStyle.regular!.buttonType != ButtonType.icon || icon != null,
          "The icon must be != null when the button type is text",
        );

  Widget child(
    ButtonType type,
    Behaviour childBehaviour,
    ButtonComponentSharedStyle sharedStyle,
  ) {
    switch (type) {
      case ButtonType.text:
        return trailingWidget != null
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconComponent(
                    icon: trailingWidget!,
                    behaviour: childBehaviour,
                    componentStyle: sharedStyle.iconStyle,
                  ),
                  const SizedBox(
                    width: MSizes.s8,
                  ),
                  Expanded(
                    child: LabelComponent(
                      behaviour: childBehaviour,
                      componentStyle: sharedStyle.textStyle,
                      text: text!,
                    ),
                  )
                ],
              )
            : LabelComponent(
                behaviour: childBehaviour,
                componentStyle: sharedStyle.textStyle,
                text: text!,
              );
      case ButtonType.icon:
        return IconComponent(
          icon: icon!,
          behaviour: childBehaviour,
          componentStyle: sharedStyle.iconStyle,
        );
    }
  }

  @override
  Widget whenRegular(
    ButtonComponentStyle style,
    ButtonComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: ElevatedButton(
        onPressed: onPressed,
        style: style.buttonStyle,
        child: child(
          style.buttonType,
          childBehaviour,
          sharedStyle,
        ),
      ),
    );
  }

  @override
  Widget whenDisabled(
    ButtonComponentStyle style,
    ButtonComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: ElevatedButton(
        onPressed: null,
        style: style.buttonStyle,
        child: child(
          style.buttonType,
          childBehaviour,
          sharedStyle,
        ),
      ),
    );
  }

  @override
  Widget whenLoading(
    ButtonComponentStyle style,
    ButtonComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: LoadingWidget(
        style: sharedStyle.loadingStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate =>
      BehaviourHelper.regularAndLoadingAndDisabled;
}
