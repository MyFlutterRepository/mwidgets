import 'package:flutter/material.dart';

import '../../../../core/component/component_style.dart';
import '../../../../exports/shared.dart';

import '../../../atoms/icon/component/icon_component_style.dart';
import '../../../atoms/label/component/label_component_style.dart';

enum ButtonType { text, icon }

class ButtonComponentStyle {
  final ButtonStyle buttonStyle;
  final ButtonType buttonType;

  ButtonComponentStyle({
    required this.buttonStyle,
    required this.buttonType,
  });
}

class ButtonComponentSharedStyle {
  final ComponentStyle<IconComponentStyle, IconComponentSharedStyle> iconStyle;
  final ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>
      textStyle;
  final LoadingStyle loadingStyle;

  ButtonComponentSharedStyle({
    required this.iconStyle,
    required this.loadingStyle,
    required this.textStyle,
  });
}
