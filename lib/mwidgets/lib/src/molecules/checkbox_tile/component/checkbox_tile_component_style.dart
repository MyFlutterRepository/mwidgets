import '../../../../core/component/component_style.dart';
import '../../../atoms/check/component/check_component_style.dart';
import '../../../atoms/label/component/label_component_style.dart';

class CheckboxTileComponentStyle {}

class CheckboxTileComponentSharedStyle {
  ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle> labelStyle;
  ComponentStyle<CheckComponentStyle, CheckComponentSharedStyle> checkStyle;

  CheckboxTileComponentSharedStyle({
    required this.labelStyle,
    required this.checkStyle,
  });
}
