import 'package:flutter/material.dart';
import '../../../../exports/tokens.dart';
import '../../../atoms/check/component/check_component.dart';
import '../../../atoms/label/component/label_component.dart';
import 'checkbox_tile_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class CheckboxTileComponent extends StatelessWidget
    with
        Component<CheckboxTileComponentStyle,
            CheckboxTileComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<CheckboxTileComponentStyle,
      CheckboxTileComponentSharedStyle> componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  /// If the checkbox is checked
  final bool initialChecked;

  /// Callback when check change
  final Function(bool)? onChange;

  /// Label of check
  final String text;

  const CheckboxTileComponent({
    required this.behaviour,
    required this.componentStyle,
    required this.text,
    this.onChange,
    this.initialChecked = false,
    this.labelSemantics,
    this.hintSemantics,
    super.key,
  });

  @override
  Widget whenRegular(
    CheckboxTileComponentStyle style,
    CheckboxTileComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          CheckComponent(
            behaviour: childBehaviour,
            componentStyle: sharedStyle.checkStyle,
            initialChecked: initialChecked,
            onChange: onChange,
          ),
          const SizedBox(
            width: MSizes.s8,
          ),
          Flexible(
            child: LabelComponent(
              behaviour: behaviour,
              componentStyle: sharedStyle.labelStyle,
              text: text,
              maxLines: 2,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget whenError(
    CheckboxTileComponentStyle style,
    CheckboxTileComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      whenRegular(
        style,
        sharedStyle,
        context,
        childBehaviour,
      );

  @override
  Widget whenDisabled(
    CheckboxTileComponentStyle style,
    CheckboxTileComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      whenRegular(
        style,
        sharedStyle,
        context,
        childBehaviour,
      );

  @override
  Widget whenLoading(
    CheckboxTileComponentStyle style,
    CheckboxTileComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      whenRegular(
        style,
        sharedStyle,
        context,
        childBehaviour,
      );

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => BehaviourHelper.exceptEmpty;
}
