import '../../atoms/check/check_specs.dart';
import '../../atoms/label/label_specs.dart';

import '../../../core/component/component_style.dart';
import 'component/checkbox_tile_component.dart';
import 'component/checkbox_tile_component_style.dart';

abstract class CheckboxTileSpecs extends CheckboxTileComponent {
  /// Put your comment here
  const CheckboxTileSpecs({
    required super.behaviour,
    required super.componentStyle,
    required super.text,
    super.onChange,
    super.initialChecked = false,
    super.labelSemantics,
    super.hintSemantics,
    super.key,
  });

  static standardStyle() => ComponentStyle<CheckboxTileComponentStyle,
          CheckboxTileComponentSharedStyle>(
        regular: CheckboxTileComponentStyle(),
        loading: CheckboxTileComponentStyle(),
        error: CheckboxTileComponentStyle(),
        disabled: CheckboxTileComponentStyle(),
        shared: CheckboxTileComponentSharedStyle(
          checkStyle: CheckSpecs.standardStyle(),
          labelStyle: LabelSpecs.h1RegularRobotStyle(),
        ),
      );
}
