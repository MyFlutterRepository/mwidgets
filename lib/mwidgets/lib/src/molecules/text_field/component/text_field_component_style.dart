import 'package:flutter/material.dart';

import '../../../../core/component/component_style.dart';
import '../../../../exports/shared.dart';
import '../../../atoms/label/component/label_component_style.dart';

class TextFieldComponentStyle {
  final bool expands;
  final TextStyle? textStyle;
  final TextAlign textAlign;
  final String obscuringCharacter = '•';
  final EdgeInsetsGeometry? contentPadding;
  final BoxConstraints? constraints;
  final InputBorder? border;
  final Color? fillColor;
  final Color? focusColor;
  final TextStyle? errorStyle;
  final TextStyle? hintStyle;
  final TextStyle? helperStyle;
  final bool? isDense;
  final TextStyle? labelStyle;
  final bool filled;
  final FloatingLabelAlignment? floatingLabelAlignment;
  final FloatingLabelBehavior? floatingLabelBehavior;
  final TextStyle? floatingLabelStyle;
  final bool isFixedLabel;
  final int? minLines;

  TextFieldComponentStyle({
    this.minLines,
    this.expands = false,
    this.textStyle,
    this.textAlign = TextAlign.start,
    this.contentPadding,
    this.constraints,
    this.border,
    this.fillColor,
    this.focusColor,
    this.errorStyle,
    this.hintStyle,
    this.helperStyle,
    this.isDense,
    this.labelStyle,
    this.filled = false,
    this.floatingLabelAlignment,
    this.floatingLabelBehavior,
    this.floatingLabelStyle,
    this.isFixedLabel = false,
  });
}

class TextFieldComponentSharedStyle {
  ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>?
      fixedLabelStyle;

  LoadingStyle loadingStyle;

  TextFieldComponentSharedStyle({
    this.fixedLabelStyle,
    required this.loadingStyle,
  });
}
