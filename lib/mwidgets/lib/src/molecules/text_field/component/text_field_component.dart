import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../../exports/shared.dart';
import '../../../../exports/tokens.dart';
import '../../../atoms/label/component/label_component.dart';
import 'text_field_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class TextFieldComponent extends StatelessWidget
    with Component<TextFieldComponentStyle, TextFieldComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<TextFieldComponentStyle, TextFieldComponentSharedStyle>
      componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  ///Function called when the input text change
  final Function(String?)? onChange;

  ///Focus control of the field
  final FocusNode? focusNode;

  ///Text controller
  final TextEditingController? controller;

  ///Enabled
  final bool? enabled;

  /// [TextField.buildCounter]
  final Widget? Function(
    BuildContext, {
    required int currentLength,
    required bool isFocused,
    required int? maxLength,
  })? buildCounter;

  /// [TextField.inputFormatters]
  final List<TextInputFormatter>? inputFormatters;

  /// [TextField.obscureText]
  final bool obscureText;

  /// [TextField.readOnly]
  final bool readOnly;

  /// [TextField.textCapitalization]
  final TextCapitalization textCapitalization;

  /// [TextField.keyboardType]
  final TextInputType? keyboardType;

  /// [InputDecoration.errorText]
  final String? errorText;

  /// [InputDecoration.hintText]
  final String? hintText;

  /// [InputDecoration.helperText]
  final String? helperText;

  /// [InputDecoration.label]
  final String? label;

  /// [InputDecoration.prefixIcon]
  final Widget? prefixIcon;

  /// [InputDecoration.suffix]
  final Widget? suffix;

  /// [TextField.maxLength]
  final int? maxLength;

  /// [TextField.maxLines]
  final int? maxLines;

  const TextFieldComponent({
    super.key,
    required this.behaviour,
    required this.componentStyle,
    this.obscureText = false,
    this.readOnly = false,
    this.textCapitalization = TextCapitalization.none,
    this.labelSemantics,
    this.hintSemantics,
    this.onChange,
    this.focusNode,
    this.controller,
    this.enabled,
    this.inputFormatters,
    this.buildCounter,
    this.keyboardType,
    this.errorText,
    this.hintText,
    this.helperText,
    this.label,
    this.prefixIcon,
    this.suffix,
    this.maxLength,
    this.maxLines,
  });

  @override
  Widget whenRegular(
    TextFieldComponentStyle style,
    TextFieldComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    assert(!style.isFixedLabel || sharedStyle.fixedLabelStyle != null);
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (style.isFixedLabel) ...[
            LabelComponent(
              behaviour: childBehaviour,
              componentStyle: sharedStyle.fixedLabelStyle!,
              text: label ?? '',
            ),
            const SizedBox(
              height: MSizes.s8,
            ),
          ],
          TextField(
            minLines: style.minLines,
            onChanged: onChange,
            controller: controller,
            maxLength: maxLength,
            maxLines: maxLines,
            enabled: enabled,
            inputFormatters: inputFormatters,
            focusNode: focusNode,
            buildCounter: buildCounter,
            obscureText: obscureText,
            expands: style.expands,
            keyboardType: keyboardType,
            style: style.textStyle,
            textAlign: style.textAlign,
            obscuringCharacter: style.obscuringCharacter,
            readOnly: readOnly,
            textCapitalization: textCapitalization,
            decoration: InputDecoration(
              contentPadding: style.contentPadding,
              constraints: style.constraints,
              border: style.border,
              fillColor: style.fillColor,
              focusColor: style.focusColor,
              errorStyle: style.errorStyle,
              errorText: errorText,
              hintStyle: style.hintStyle,
              hintText: hintText,
              helperText: helperText,
              helperStyle: style.helperStyle,
              isDense: style.isDense,
              labelText: style.isFixedLabel ? null : label,
              labelStyle: style.labelStyle,
              prefixIcon: prefixIcon,
              suffix: suffix,
              filled: style.filled,
              floatingLabelAlignment: style.floatingLabelAlignment,
              floatingLabelBehavior: style.floatingLabelBehavior,
              floatingLabelStyle: style.floatingLabelStyle,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget whenDisabled(
    TextFieldComponentStyle style,
    TextFieldComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    assert(!style.isFixedLabel || sharedStyle.fixedLabelStyle != null);
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (style.isFixedLabel) ...[
            LabelComponent(
              behaviour: childBehaviour,
              componentStyle: sharedStyle.fixedLabelStyle!,
              text: label ?? '',
            ),
            const SizedBox(
              height: MSizes.s8,
            ),
          ],
          TextField(
            onChanged: onChange,
            controller: controller,
            maxLength: maxLength,
            maxLines: maxLines,
            enabled: false,
            inputFormatters: inputFormatters,
            focusNode: focusNode,
            buildCounter: buildCounter,
            obscureText: obscureText,
            expands: style.expands,
            keyboardType: keyboardType,
            style: style.textStyle,
            textAlign: style.textAlign,
            obscuringCharacter: style.obscuringCharacter,
            readOnly: readOnly,
            textCapitalization: textCapitalization,
            decoration: InputDecoration(
              contentPadding: style.contentPadding,
              constraints: style.constraints,
              border: style.border,
              fillColor: style.fillColor,
              focusColor: style.focusColor,
              errorStyle: style.errorStyle,
              errorText: errorText,
              hintStyle: style.hintStyle,
              hintText: hintText,
              helperText: helperText,
              helperStyle: style.helperStyle,
              isDense: style.isDense,
              labelText: style.isFixedLabel ? null : label,
              labelStyle: style.labelStyle,
              prefixIcon: prefixIcon,
              suffix: suffix,
              filled: style.filled,
              floatingLabelAlignment: style.floatingLabelAlignment,
              floatingLabelBehavior: style.floatingLabelBehavior,
              floatingLabelStyle: style.floatingLabelStyle,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget whenLoading(
    TextFieldComponentStyle style,
    TextFieldComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: LoadingWidget(style: sharedStyle.loadingStyle),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate =>
      BehaviourHelper.regularAndLoadingAndDisabled;
}
