import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';
import '../../atoms/label/label_specs.dart';

import '../../../core/component/component_style.dart';
import 'component/text_field_component.dart';
import 'component/text_field_component_style.dart';

abstract class TextFieldSpecs extends TextFieldComponent {
  /// Put your comment here
  const TextFieldSpecs({
    super.key,
    required super.behaviour,
    required super.componentStyle,
    super.obscureText = false,
    super.readOnly = false,
    super.textCapitalization = TextCapitalization.none,
    super.labelSemantics,
    super.hintSemantics,
    super.onChange,
    super.focusNode,
    super.controller,
    super.enabled,
    super.inputFormatters,
    super.buildCounter,
    super.keyboardType,
    super.errorText,
    super.hintText,
    super.helperText,
    super.label,
    super.prefixIcon,
    super.suffix,
    super.maxLength,
    super.maxLines,
  });

  static standardStyle() =>
      ComponentStyle<TextFieldComponentStyle, TextFieldComponentSharedStyle>(
        regular: TextFieldComponentStyle(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.background2,
          hintStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.blue1,
          ),
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: MSizes.s16,
          ),
          isFixedLabel: true,
        ),
        loading: TextFieldComponentStyle(),
        disabled: TextFieldComponentStyle(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.grey,
          hintStyle: MTextStyles.body1RegularRobot,
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: MSizes.s16,
          ),
          isFixedLabel: true,
        ),
        shared: TextFieldComponentSharedStyle(
          fixedLabelStyle: LabelSpecs.h1RegularRobotStyle(),
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(
              double.maxFinite,
              MSizes.s56,
            ),
          ),
        ),
      );

  static expandedStandardStyle() =>
      ComponentStyle<TextFieldComponentStyle, TextFieldComponentSharedStyle>(
        regular: TextFieldComponentStyle(
          minLines: 16,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.background2,
          hintStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.blue1,
          ),
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.all(
            MSizes.s16,
          ),
          isFixedLabel: true,
        ),
        loading: TextFieldComponentStyle(),
        disabled: TextFieldComponentStyle(
          minLines: 16,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.grey,
          hintStyle: MTextStyles.body1RegularRobot,
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.all(MSizes.s16),
          isFixedLabel: true,
        ),
        shared: TextFieldComponentSharedStyle(
          fixedLabelStyle: LabelSpecs.h1RegularRobotStyle(),
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(
              double.maxFinite,
              MSizes.s56,
            ),
          ),
        ),
      );

  static withoutLabelStyle() =>
      ComponentStyle<TextFieldComponentStyle, TextFieldComponentSharedStyle>(
        regular: TextFieldComponentStyle(
          floatingLabelStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.transparent,
          ),
          labelStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.transparent,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.background2,
          hintStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.blue1,
          ),
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.all(MSizes.s16),
        ),
        loading: TextFieldComponentStyle(),
        disabled: TextFieldComponentStyle(
          floatingLabelStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.transparent,
          ),
          labelStyle: MTextStyles.body1RegularRobot.copyWith(
            color: MColors.transparent,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(MSizes.s4),
            borderSide: BorderSide.none,
          ),
          fillColor: MColors.grey,
          hintStyle: MTextStyles.body1RegularRobot,
          textStyle: MTextStyles.body2RegularRobot,
          filled: true,
          contentPadding: const EdgeInsets.all(MSizes.s16),
        ),
        shared: TextFieldComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(
              double.maxFinite,
              MSizes.s56,
            ),
          ),
        ),
      );
}
