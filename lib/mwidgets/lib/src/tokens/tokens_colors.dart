import 'package:flutter/material.dart';

abstract class MColors {
  MColors._();

  static const transparent = Colors.transparent;

  static const background1 = Color(0xFFF2F2F2);

  static const white = Color(0xFFFFFFFF);

  static const blue1 = Color(0xFF1E19EB);

  static const grey = Color.fromARGB(255, 188, 193, 197);

  static const background2 = Color(0xFFBBD6FF);

  static const blue2 = Color(0xFF4745B6);

  static const black = Color(0xFF000000);

  static const red1 = Color.fromARGB(255, 245, 128, 128);
}
