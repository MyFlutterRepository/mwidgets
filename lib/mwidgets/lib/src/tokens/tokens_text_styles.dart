import 'package:flutter/material.dart';

abstract class MTextStyles {
  MTextStyles._();

  /// [fontFamily] 'RobotoFlex', [fontSize] 24, [fontWeight] 600
  static const TextStyle h2SemiboldRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 24,
    fontWeight: FontWeight.w600,
    height: 1.2,
  );

  /// [fontFamily] 'RobotoFlex', [fontSize] 20, [fontWeight] 400
  static const TextStyle h1RegularRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 20,
    fontWeight: FontWeight.w400,
    height: 1.2,
  );

  /// [fontFamily] 'RobotoFlex', [fontSize] 20, [fontWeight] 400
  static const TextStyle h1RegularUnderlineRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 20,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.underline,
    height: 1.2,
  );

  /// [fontFamily] 'RobotoFlex', [fontSize] 14, [fontWeight] 400
  static const TextStyle body1RegularRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 14,
    fontWeight: FontWeight.w400,
    height: 1.2,
  );

  /// [fontFamily] 'RobotoFlex', [fontSize] 16, [fontWeight] 700
  static const TextStyle body2MediumRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 16,
    fontWeight: FontWeight.w700,
    height: 1.2,
  );

  /// [fontFamily] 'RobotoFlex', [fontSize] 16, [fontWeight] 400
  static const TextStyle body2RegularRobot = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 16,
    fontWeight: FontWeight.w400,
    height: 1.2,
  );
}
