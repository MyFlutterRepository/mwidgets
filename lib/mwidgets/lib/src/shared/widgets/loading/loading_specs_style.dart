import 'package:flutter/material.dart';

import '../../../../exports/shared.dart';
import '../../../../exports/tokens.dart';

class LoadingSpecs {
  static standardStyle(Size size) => LoadingStyle(
        size: size,
        baseColor: MColors.background2,
        highlightColor: MColors.blue1,
        borderRadius: BorderRadius.circular(8),
      );
}
