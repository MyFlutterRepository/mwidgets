extension TextDecorations on String {
  String bold() {
    return replaceFirst(RegExp(r'^\*{2}'), "");
  }

  String italic() {
    return replaceFirst(RegExp(r'^\_'), "");
  }

  String link() {
    return replaceFirst(RegExp(r'^\['), "");
  }

  String url() {
    return replaceAll(RegExp(r"\(\'+[A-zÀ-ú0-9!@#$%&()><:\-`.+,/\s]+\'\)"), "");
  }
}
