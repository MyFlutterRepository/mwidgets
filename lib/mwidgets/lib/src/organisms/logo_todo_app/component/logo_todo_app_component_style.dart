import '../../../../core/component/component_style.dart';
import '../../../molecules/checkbox_tile/component/checkbox_tile_component_style.dart';

class LogoTodoAppComponentStyle {}

class LogoTodoAppComponentSharedStyle {
  ComponentStyle<CheckboxTileComponentStyle, CheckboxTileComponentSharedStyle>
      checkboxStyle;

  LogoTodoAppComponentSharedStyle({
    required this.checkboxStyle,
  });
}
