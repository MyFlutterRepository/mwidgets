import 'package:flutter/material.dart';
import '../../../../exports/tokens.dart';
import '../../../molecules/checkbox_tile/component/checkbox_tile_component.dart';
import 'logo_todo_app_component_style.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class LogoTodoAppComponent extends StatelessWidget
    with Component<LogoTodoAppComponentStyle, LogoTodoAppComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<LogoTodoAppComponentStyle,
      LogoTodoAppComponentSharedStyle> componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  const LogoTodoAppComponent({
    required this.behaviour,
    required this.componentStyle,
    this.labelSemantics,
    this.hintSemantics,
    super.key,
  });

  @override
  Widget whenRegular(
    LogoTodoAppComponentStyle style,
    LogoTodoAppComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CheckboxTileComponent(
            behaviour: childBehaviour,
            componentStyle: sharedStyle.checkboxStyle,
            text: 'To',
          ),
          const SizedBox(height: MSizes.s8),
          CheckboxTileComponent(
            behaviour: childBehaviour,
            componentStyle: sharedStyle.checkboxStyle,
            text: 'Do',
            initialChecked: true,
          ),
          const SizedBox(height: MSizes.s8),
          CheckboxTileComponent(
            behaviour: childBehaviour,
            componentStyle: sharedStyle.checkboxStyle,
            text: 'List',
          ),
        ],
      ),
    );
  }

  @override
  Widget whenLoading(
    LogoTodoAppComponentStyle style,
    LogoTodoAppComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) =>
      const SizedBox();

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => {
        Behaviour.disabled: Behaviour.regular,
        Behaviour.loading: Behaviour.regular,
        Behaviour.empty: Behaviour.regular,
        Behaviour.error: Behaviour.regular,
      };
}
