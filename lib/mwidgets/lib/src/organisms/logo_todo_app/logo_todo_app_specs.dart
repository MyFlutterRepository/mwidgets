import '../../../core/component/component_style.dart';
import '../../molecules/checkbox_tile/checkbox_tile_specs.dart';
import 'component/logo_todo_app_component.dart';
import 'component/logo_todo_app_component_style.dart';

abstract class LogoTodoAppSpecs extends LogoTodoAppComponent {
  /// Put your comment here
  const LogoTodoAppSpecs({
    required super.behaviour,
    required super.componentStyle,
    super.labelSemantics,
    super.hintSemantics,
    super.key,
  });

  static standardStyle() => ComponentStyle<LogoTodoAppComponentStyle,
          LogoTodoAppComponentSharedStyle>(
        regular: LogoTodoAppComponentStyle(),
        shared: LogoTodoAppComponentSharedStyle(
          checkboxStyle: CheckboxTileSpecs.standardStyle(),
        ),
      );
}
