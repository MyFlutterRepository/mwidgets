import 'package:flutter/material.dart';

import '../../../../core/component/component_style.dart';
import '../../../../exports/shared.dart';
import '../../../atoms/label/component/label_component_style.dart';
import '../../../molecules/checkbox_tile/component/checkbox_tile_component_style.dart';

class ExpandableListComponentStyle {
  final Color? upperDividerColor;
  final double? upperDividerThickness;
  final Color? downDividerColor;
  final double? downDividerThickness;

  ExpandableListComponentStyle({
    this.upperDividerColor,
    this.upperDividerThickness,
    this.downDividerColor,
    this.downDividerThickness,
  });
}

class ExpandableListComponentSharedStyle {
  final ComponentStyle<LabelComponentStyle, LabelComponentSharedStyle>
      nameStyle;
  final ComponentStyle<CheckboxTileComponentStyle,
      CheckboxTileComponentSharedStyle> checkboxTileStyle;

  final LoadingStyle loadingStyle;
  ExpandableListComponentSharedStyle({
    required this.loadingStyle,
    required this.checkboxTileStyle,
    required this.nameStyle,
  });
}
