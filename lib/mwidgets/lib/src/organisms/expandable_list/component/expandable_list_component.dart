import 'dart:math';

import 'package:flutter/material.dart';
import '../../../../exports/shared.dart';
import '../../../../exports/tokens.dart';
import '../../../atoms/label/component/label_component.dart';
import '../../../molecules/checkbox_tile/component/checkbox_tile_component.dart';
import '../expandable_list_item.dart';
import '../checkbox_item.dart';
import 'expandable_list_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class ExpandableListComponent extends StatelessWidget
    with
        Component<ExpandableListComponentStyle,
            ExpandableListComponentSharedStyle> {
  /// Component Behaviour
  final Behaviour behaviour;

  /// Component style
  final ComponentStyle<ExpandableListComponentStyle,
      ExpandableListComponentSharedStyle> componentStyle;

  /// Label for accessibility
  final String? labelSemantics;

  /// Hint for accessibility
  final String? hintSemantics;

  /// List Contents
  final List<ExpandableListItem> lists;

  /// CallBack called when one check is pressed
  final void Function(List<ExpandableListItem>)? onCheckPressed;

  /// If the list must shrinkWrap
  final bool shrinkWrap;

  /// Scrollable Physics of the list
  final ScrollPhysics? scrollPhysics;

  const ExpandableListComponent({
    required this.behaviour,
    required this.componentStyle,
    required this.lists,
    this.onCheckPressed,
    this.labelSemantics,
    this.hintSemantics,
    this.shrinkWrap = true,
    this.scrollPhysics,
    super.key,
  });

  Widget _buidListItems(
    CheckboxItem item,
    CheckboxItem last,
    ExpandableListComponentStyle style,
    ExpandableListComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    if (last == item) {
      return Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: MSizes.s32,
        ),
        child: Divider(
          color: style.downDividerColor,
          thickness: style.downDividerThickness,
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(
        MSizes.s32,
        0,
        0,
        MSizes.s8,
      ),
      child: CheckboxTileComponent(
        behaviour: childBehaviour,
        componentStyle: sharedStyle.checkboxTileStyle,
        text: item.name,
        initialChecked: item.isChecked,
      ),
    );
  }

  @override
  Widget whenRegular(
    ExpandableListComponentStyle style,
    ExpandableListComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: ListView.builder(
        shrinkWrap: shrinkWrap,
        physics: scrollPhysics,
        itemCount: lists.length,
        itemBuilder: (context, index) {
          final list = lists[index];
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(
                color: style.upperDividerColor,
                thickness: style.upperDividerThickness,
                height: 0,
              ),
              ExpansionTile(
                tilePadding: const EdgeInsets.only(left: MSizes.s16),
                expandedCrossAxisAlignment: CrossAxisAlignment.start,
                title: LabelComponent(
                  behaviour: childBehaviour,
                  text: list.listName,
                  componentStyle: sharedStyle.nameStyle,
                ),
                children: list.listItens
                    .map(
                      (item) => _buidListItems(
                        item,
                        list.listItens.last,
                        style,
                        sharedStyle,
                        context,
                        childBehaviour,
                      ),
                    )
                    .toList(),
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  Widget whenLoading(
    ExpandableListComponentStyle style,
    ExpandableListComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    int listLength = Random().nextInt(6);
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: ListView.builder(
        shrinkWrap: shrinkWrap,
        physics: scrollPhysics,
        itemCount: listLength,
        itemBuilder: (context, _) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(
                color: style.upperDividerColor,
                thickness: style.upperDividerThickness,
                height: MSizes.s16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: MSizes.s16),
                child: LoadingWidget(style: sharedStyle.loadingStyle),
              ),
              const SizedBox(
                height: MSizes.s16,
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => BehaviourHelper.regularAndLoading;
}
