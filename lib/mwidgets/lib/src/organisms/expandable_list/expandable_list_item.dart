import 'checkbox_item.dart';

class ExpandableListItem {
  final String listName;
  final List<CheckboxItem> listItens;
  ExpandableListItem({
    required this.listName,
    required this.listItens,
  });
}
