import 'package:flutter/material.dart';
import '../../../exports/shared.dart';
import '../../../exports/tokens.dart';
import '../../atoms/label/label_specs.dart';
import '../../molecules/checkbox_tile/checkbox_tile_specs.dart';

import '../../../core/component/component_style.dart';
import 'component/expandable_list_component.dart';
import 'component/expandable_list_component_style.dart';

abstract class ExpandableListSpecs extends ExpandableListComponent {
  /// Put your comment here
  const ExpandableListSpecs({
    required super.behaviour,
    required super.componentStyle,
    required super.lists,
    super.labelSemantics,
    super.hintSemantics,
    super.shrinkWrap = true,
    super.scrollPhysics,
    super.key,
  });

  static standardStyle() => ComponentStyle<ExpandableListComponentStyle,
          ExpandableListComponentSharedStyle>(
        regular: ExpandableListComponentStyle(
          downDividerColor: MColors.black,
          downDividerThickness: MSizes.s1,
          upperDividerColor: MColors.black,
          upperDividerThickness: MSizes.s1,
        ),
        loading: ExpandableListComponentStyle(
          upperDividerColor: MColors.black,
          upperDividerThickness: MSizes.s1,
        ),
        shared: ExpandableListComponentSharedStyle(
          loadingStyle: LoadingSpecs.standardStyle(
            const Size(double.infinity, MSizes.s16),
          ),
          nameStyle: LabelSpecs.body2MediumRobotoStyle(),
          checkboxTileStyle: CheckboxTileSpecs.standardStyle(),
        ),
      );
}
