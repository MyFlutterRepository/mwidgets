class CheckboxItem {
  final String name;
  final bool isChecked;

  CheckboxItem({
    required this.name,
    required this.isChecked,
  });
}
