# Atomic Design

- Responsável por criar o __Component__ e definir seu __ComponentStyle__.

- Responsável por criar as customizações do __ComponentStyle__ e construir as variações filhas do __Component__ que serão exportadas do Design System.

<br>

## Criando um novo __Component__ e definindo seu __ComponentStyle__
<br>

1 - Criar uma pasta ```component``` com dois arquivos: ```nome_component.dart``` e ```nome_component_style.dart```

<br>


2 - Em ```nome_component.dart``` você deve implementar um Widget Stateless ou Statefull com herança múltipla (_via mixin - operador "with"_) da classe __Component<Style, SharedStyle>__

```dart 

class LabelComponent extends StatelessWidget with  Component<LabelStyle, LabelSharedStyle> {}

``` 
<br>

3 - Em  ```nome_component_style.dart``` você deve definir duas classes de estilo para o seu componente:
__NomeStyle__ (para estilos do próprio componente) e __NomeSharedStyle__ (para estilos de outros componentes que ali serão utilizados).

``` dart

class LabelStyle {
  final TextStyle textStyle;
  final EdgeInsets padding;

  LabelStyle({
    required this.textStyle,
    required this.padding,
  });
}

class LabelSharedStyle {
  final LoadingStyle loadingStyle;

  LabelSharedStyle({
    required this.loadingStyle,
  });

```

## Estruturando um novo componente
<br>

No componente criado em ```nome_component.dart``` deve-se montar a seguinte estrutura:

1 - Criar obrigatoriamente os atributos como _required_: 

  -  Behaviour

  - ComponentStyle

  <br>


Criar também os demais atributos que o seu componente for utilizar:

```dart

  /// Comportamento do Componente
  final Behaviour behaviour;

  /// Estilo do Componente
  final ComponentStyle<LabelStyle, LabelSharedStyle> componentStyle;

  /// Chave Identificadora do Componente
  final Key labelKey;

  /// Texto do Componente
  final String text;
  
  ...

  /// Construtor
    const LabelComponent({
    required this.behaviour,
    required this.componentStyle,
    required this.labelKey,
    required this.text,
    this.textAlign,
    this.textOverflow,
    this.textDirection,
    this.maxLines,
    this.labelSemantics,
    this.hintSemantics,
  }) : super(key: labelKey);

```
<br>

2 - Implementar as funções:

-  Função __build__ sobrescrevendo a chamada -> __super.render(behaviour, context, styles)__  

<br>

Os parâmetros passados no __super.render__, virão dos atributos que definimos como required no passo anterior.

```dart
  
  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

```
<br>

-  Sobrescrever a função ->  __get delegate__ passando __null__ caso queira implementar a renderização de todos ```behaviours``` possíveis ou passar o __BehaviourHelper__ que faz o tratamento para renderizar somente os ```behaviours``` que desejar.

<br>


```dart
  
  ASSIM: 

  @override
  Map<Behaviour, Behaviour>? get delegate => null;


  OU ASSIM:


  @override
  Map<Behaviour, Behaviour>? get delegate => BehaviourHelper.regularAndLoading;

```
<br>


3 - Sobrescrever as funções das renderizações desejadas a partir dos comportamentos do componente:

- whenRegular
- whenLoading
- whenError
- whenEmpty
- whenDisabled

<br>

```dart

  @override
  Widget whenRegular(
    LabelStyle style,
    LabelSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Padding(
        padding: style.padding,
        child: Text(
          text,
          key: labelKey,
          style: style.textStyle,
          maxLines: maxLines,
          textAlign: textAlign,
          overflow: textOverflow,
          textDirection: textDirection,
        ),
      ),
    );
  }

  @override
  Widget whenLoading(
    LabelStyle style,
    LabelSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return LoadingWidget(style: sharedStyle.loadingStyle);
  }


```

<br>


## Você criou o __Component__ e definiu seu __ComponentStyle__, agora vamos criar as customizações do __ComponentStyle__ e construir as variações filhas do __Component__ que serão exportadas do Design System:

<br>

1 - Criar um arquivo ```nome_styles.dart``` onde serão criadas as customizações do ComponentStyle:

```dart 

class LabelStyles {
  ///
  /// body1Style
  ///
  static ComponentStyle<LabelStyle, LabelSharedStyle> body1Style() =>
      ComponentStyle<LabelStyle, LabelSharedStyle>(
        shared: LabelSharedStyle(
          loadingStyle: LoadingStyle(
            size: const Size(110, 24),
            baseColor: QColors().loadingBackground,
            highlightColor: QColors().loadingShimmer,
          ),
        ),
        regular: LabelStyle(
          textStyle: TokensTextStyles().body1,
          padding: const EdgeInsets.all(15.0),
        ),
      );

  ///
  /// headline1Style
  ///
  static ComponentStyle<LabelStyle, LabelSharedStyle> headline1Style() =>
      ComponentStyle<LabelStyle, LabelSharedStyle>(
        shared: LabelSharedStyle(
          loadingStyle: LoadingStyle(
            size: const Size(110, 24),
            baseColor: QColors().loadingBackground,
            highlightColor: QColors().loadingShimmer,
          ),
        ),
        regular: LabelStyle(
          textStyle: TokensTextStyles().headline1,
          padding: const EdgeInsets.all(15.0),
        ),
      );
}

```

2 - Criar um arquivo ```nome.dart``` onde serão construidas as variações filhas do Component que serão exportadas do Design System:

```dart

class Label extends LabelComponent {
  ///
  /// body1
  ///
  Label.body1({
    required super.labelKey,
    required super.behaviour,
    required super.text,
    super.textAlign,
    super.textOverflow,
    super.textDirection,
    super.maxLines,
    super.labelSemantics,
    super.hintSemantics,
  }) : super(componentStyle: LabelStyles.body1Style());

  ///
  /// headline1
  ///
  Label.headline1({
    required super.labelKey,
    required super.behaviour,
    required super.text,
    super.textAlign,
    super.textOverflow,
    super.textDirection,
    super.maxLines,
    super.labelSemantics,
    super.hintSemantics,
  }) : super(componentStyle: LabelStyles.headline1Style());
}

```

3 - Colocar o export o ```nome.dart``` no export dos atoms, molecules ou organisms que ficam na pasta ```exports```.


4 - Criar o __Fixture__ e o __Teste de Widget__ para todos os Componentes criados. 